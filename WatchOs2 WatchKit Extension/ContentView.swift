import SwiftUI

struct ContentView: View {
    
    // General Variables
    @State var selected1 = false
    @State var disableButtons = true
    @State var selected2 = false
    @State var selected3 = false
    @State var opacity1 = 0.5
    @State var opacity2 = 0.5
    @State var opacity3 = 0.5
    @State var RandomArray: [Bool] = []
    @State var userArray: [Bool] = [false, false, false]
    @State var disableStart = false
    
    
    //  Timer Variables
    let timer = Timer.publish(every: 1, on: .current, in: .common)
    @State var seconds = 0
    
    // Start Button variables
    @State var textButton = "Start"
    
    var body: some View {
        ZStack {
            
            
            VStack {
                Button(action: {
                    
                    // Function to detect what appens on pressing button1 or button2
                    func buttonsPressed(){
                        withAnimation{
                            self.textButton = "Copy the sequence"
                        }
                        
                        if self.selected1 == true {
                            withAnimation{
                                self.opacity1 = 0.5
                            }
                            
                        }
                        if self.selected2 == true{
                            withAnimation{
                                self.opacity2 = 1
                            }
                        }
                        if self.selected3 == true {
                            withAnimation{
                                self.opacity3 = 1
                            }
                        }
                    }
                    
                    // Generate random values
                    self.selected1 = Bool.random()
                    self.selected2 = Bool.random()
                    self.selected3 = Bool.random()
                    
                    // Case False - False (NOT Allowed)
                    if self.selected1 == false && self.selected2 == false && self.selected3 == false {
                        print ("\n------false / false------\n")
                        
                        self.selected2 = !self.selected2
                        
                        print("Button 2 is now \(self.selected2)")
                        
                        buttonsPressed()
                    }
                    
                    buttonsPressed()
                    self.RandomArray.append(self.selected1)
                    self.RandomArray.append(self.selected2)
                    self.RandomArray.append(self.selected3)
                    print("ARRAY: \(self.RandomArray)")
                    
                    self.disableStart = true
                    self.timer.connect()
                    
                }) {
                    Text(textButton)
                }
                .disabled(disableStart)
                
                
                
                
                
                
                //         UI
                HStack{
                    Button(action: {
                        print("1")
                        self.userArray.remove(at: 0)
                        self.userArray.insert(true, at: 0)
                        self.opacity1 = 1
                        
                        if self.userArray == self.RandomArray{
                            print("uguali")
                        }else{
                            print("diversi")
                        }
                        if self.selected1 == true {
                            withAnimation{
                                self.opacity1 = 1
                            }
                        } else {
                            withAnimation{
                                
                            }
                        }
                    }) {
                        Circle()
                            .fill(Color.red)
                            .frame(width: 50, height: 50)
                            .onReceive(timer){_ in
                                self.seconds += 1
                                if self.seconds == 2{
                                    withAnimation{
                                        self.opacity1 = 0.2
                                    }
                                } else if self.seconds == 3{
                                    withAnimation{
                                        self.opacity1 = 1
                                    }
                                    self.timer.connect().cancel()
                                    self.disableButtons = false
                                }
                        }
                    }
                    .frame(width: 50, height: 50)
                    .opacity(self.opacity1)
                    .disabled(disableButtons)
                    Spacer().frame(width: 30)
                    Button(action: {
                        print("2")
                        self.userArray.remove(at: 1)
                        self.userArray.insert(true, at: 1)
                        print("User Arrray: \(self.userArray)")
                        
                        if self.userArray == self.RandomArray{
                            print("uguali")
                        }else{
                            print("diversi")
                        }
                    }) {
                        Circle()
                            .fill(Color.blue)
                            .frame(width: 50, height: 50)
                            .onReceive(timer){_ in
                                if self.seconds == 2{
                                    withAnimation{
                                        self.opacity2 = 0.2
                                    }
                                } else if self.seconds == 3{
                                    withAnimation(){
                                        self.opacity2 = 1
                                    }
                                }
                        }
                    }
                    .frame(width: 50, height: 50)
                    .opacity(self.opacity2)
                    .disabled(disableButtons)
                }
                
                Button(action: {
                    print("3")
                    self.userArray.remove(at: 2)
                    self.userArray.insert(true, at: 2)
                    print("User Arrray: \(self.userArray)")
                    
                    if self.userArray == self.RandomArray{
                        print("uguali")
                    }else{
                        print("diversi")
                    }
                }) {
                    Circle()
                        .fill(Color.green)
                        .frame(width: 50, height: 50)
                        .onReceive(timer){_ in
                            if self.seconds == 2{
                                withAnimation{
                                    self.opacity3 = 0.2
                                }
                            } else if self.seconds == 3{
                                withAnimation(){
                                    self.opacity3 = 1
                                }
                            }
                    }
                }
                .frame(width: 50, height: 50)
                    
                .opacity(self.opacity3)
                .disabled(disableButtons)
            }
            
        }
        
    }
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        
        
    }
}

