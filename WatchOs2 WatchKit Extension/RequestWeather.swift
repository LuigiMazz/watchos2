//
//  RequestWeather.swift
//  WatchOs2 WatchKit Extension
//
//  Created by Luigi Mazzarella on 12/01/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation
import CoreLocation

//let apiKey = "afd0e7890f98c3e219dd9520b1722ea9"
//var lat = 41.0842
//var lon = 14.3358
//
//func fetchCurrentWeatherData() {
//    let key = "afd0e7890f98c3e219dd9520b1722ea9"
//    
//    let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
//    let endpoint = "https://api.openweathermap.org/data/2.5/forecast?lat=\(lat)&lon=\(lon)&units=metric&appid=\(key)"
//    let safeURLString = endpoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//    
//    guard let endpointURL = URL(string: safeURLString!) else {
//        print("The URL is invalid")
//        return
//    }
//    
//    var request = URLRequest(url: endpointURL)
//    request.httpMethod = "GET"
//    
//    //The datatask can also be run the the URL directly if no specific configuration is required
//    //response and error are not used in the closure, this could be improved to provide feedback to users.
//    let dataTask = session.dataTask(with: request) { (data, response, error) in
//        
//        guard let jsonData = data else {
//            print("The payload is invalid")
//            return
//        }
//        let decoder = JSONDecoder()
//        do {
//            
//            let forecastInfo = try decoder.decode(WeatherAPI.self, from: jsonData)
//            print("weather JSON decoded")
//            // Quando il JSON è stato decodificato, setta sia forecast che list, che sono le due variabili all'inizio del view controller
//            forecast = forecastInfo
//            
// 
//        } catch let error {
//            print(error)
//        }
//    }
//    dataTask.resume()
//}
//
//fileprivate var forecast: WeatherAPI? {
//    
//    // L'ho strutturato in modo che quando viene cambiata la location e viene settato questo forecast perché l'API l'ha trovato, si fa tutte queste operazioni. L'ho messo in un DispatchQueue.main perché il codice per modificare le cose della UI sono da eseguire sul thread principale
//    didSet {
//        DispatchQueue.main.async {
//            guard let forecastInfo = forecast else { return  }
//            var name = forecastInfo.name
//          
//       
//        }
//    }
//}
